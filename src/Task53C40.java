import Model.Circle;
import Model.Rectangle;

public class Task53C40 {
    public static void main(String[] args) throws Exception {
        Circle circle = new Circle(2.0);
        Rectangle rectangle = new Rectangle(4, 6);
        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println("Dien tich hinh tron: " + circle.getArea());
        System.out.println("Chu vi hinh tron: " + circle.getPerimeter());
        System.out.println("Dien tich hinh chu nhat: " + rectangle.getArea());
        System.out.println("Chu vi hinh chu nhat: " + rectangle.getPerimeter());
    }
}
